var dadosFatura = [];
var monthsList = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho",
    "Julho", "Augosto", "Setembro", "Outubro", "Novembro", "Dezembro"
];

window.onload = function () {
    console.log('Onload disparado');
    var tBody = document.getElementsByTagName('tbody')[0].children;
    this.mountData(tBody);
    this.createTable();
    this.removeElement('tb_fatura');
}



function mountData(tBody) {
    for (var tr in tBody) {
        if (tBody.hasOwnProperty(tr)) {
            const data = {};
            data.lancamento = tBody[tr].children[0].innerText;
            data.value = tBody[tr].children[1].innerText;
            data.mounth = parseInt(tBody[tr].children[2].innerText);
            data.mounthName = this.monthsList[parseInt(tBody[tr].children[2].innerText) - 1];
            dadosFatura.push(data);
        }
    }
}

function sortData(data) {
    data.sort((a, b) => a.mounth - b.mounth);
}

function createTable() {
    
    monthsList.forEach(mounth => {
        if (dadosFatura.find(data => data.mounthName === mounth)) {
            var h1 = document.createElement('h1');
            h1.appendChild(document.createTextNode(mounth));
            h1.classList.add('align-middle');

            var table = document.createElement('table');
            table.classList.add('table');
            table.innerHTML = `<thead>        
                <tr>
                   <th scope="col">Lancamento</th>
                   <th scope="col">Valor</th>            
                </tr>
            </thead>`;

            table.appendChild(this.createRow(dadosFatura.filter(data => data.mounthName === mounth)));

            document.body.appendChild(h1);
            document.body.appendChild(table);
        }
    });
}

function createRow(dataList) {
    let tBody = document.createElement('tbody');
    let totalValue = 0;
    let html = '';
    dataList.forEach(data => {

        html = html + `
        <tr>
            <td>${data.lancamento}</th>
            <td>${data.value}</th>            
        </tr>`
        totalValue = totalValue + parseFloat(data.value.replace('R$ ', '').replace(",","."));
    });
console.log(totalValue)
    html = html + `<tr>
               <td class="font-weight-bold">Valor Total</th> 
               <td class="font-weight-bold">${totalValue.toLocaleString('pt-BR', {  minimumFractionDigits: 2, style: 'currency', currency: 'BRL' })}</th>         
            </tr>`;
    tBody.innerHTML = html;

    return tBody;
}

function removeElement(elementId) {
    const el = document.getElementById(elementId);
    el.parentNode.removeChild(el);
}